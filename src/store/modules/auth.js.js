import authService from '../../services/auth'

// initial state
const state = () => ({
  user: null
})

const getters = {
  isAuthenticated(state) {
    return state.user ? true : false
  }
}

const actions = {
  login({ commit }, val) {
    return authService.login(val).then((user) => commit('setUser', user))
  },
  logout({ commit }, val) {
    return authService.logout(val).then(() => commit('setUser', null))
  }
}

const mutations = {
  setUser(state, val) {
    state.user = val
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
