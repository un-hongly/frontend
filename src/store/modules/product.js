import productService from '../../services/product'

// initial state
const state = () => ({
  items: []
})

const getters = {}

const actions = {
  getAllProducts({ commit }, params) {
    return productService
      .getAll(params)
      .then((products) => commit('setProducts', products))
  },

  updateById({ commit, state }, value) {
    const products = state.items
    const index = products.findIndex((el) => el.id === value.id)
    products.splice(index, 1, value)
    commit('setProducts', products)
    return productService.updateById()
  },

  deleteById({ commit, state }, id) {
    const products = state.items
    const index = state.items.findIndex((el) => el.id === id)
    products.splice(index, 1)
    commit('setProducts', products)
    return productService.deleteById()
  },

  create({ commit, state }, value) {
    commit('setProducts', [value, ...state.items])
  }
}

const mutations = {
  setProducts(state, products) {
    state.items = products
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
