import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import product from './modules/product'
import auth from './modules/auth.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    product
  },
  plugins: [
    createPersistedState({
      paths: ['auth.user']
    })
  ]
})
