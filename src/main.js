import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import uuid from './plugins/vue-uuid'
import notify from './plugins/vue-notification'
import utility from './plugins/utility'

import DefaultLayout from './layouts/DefaultLayout.vue'
import FullPageLayout from './layouts/FullPageLayout.vue'

Vue.config.productionTip = false
Vue.component('default-layout', DefaultLayout)
Vue.component('fullpage-layout', FullPageLayout)
Vue.use(utility)

new Vue({
  router,
  store,
  vuetify,
  notify,
  uuid,
  render: (h) => h(App)
}).$mount('#app')
