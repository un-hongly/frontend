import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/products',
    meta: {
      layout: 'default',
      auth: true
    }
  },
  {
    path: '/products',
    name: 'ProductManagementPage',
    component: () => import('../views/products/ProductManagementPage'),
    meta: {
      layout: 'default',
      auth: true
    }
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: () => import('../views/auths/LoginPage.vue'),
    meta: { layout: 'fullpage' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const isRequiredAuth = to.meta.auth
  const isAuthenticated = store.getters['auth/isAuthenticated']
  if (isRequiredAuth && !isAuthenticated) next({ path: '/login' })
  else next()
})

export default router
