import users from '../data/users.json'
export default {
  login: async ({ username, password }) => {
    await new Promise((res) => setTimeout(res, 1000))
    const user = users.find((el) => el.username === username)
    if (!user) throw new Error('Incorrect username!')
    if (user.password !== password) throw new Error('Incorrect password!')
    return user
  },
  logout: async () => {
    return null
  }
}
