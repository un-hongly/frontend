export default {
  upload: (file) => {
    return new Promise((resolve) => {
      const reader = new FileReader()
      reader.addEventListener(
        'load',
        function () {
          resolve(reader.result)
        },
        false
      )
      reader.readAsDataURL(file)
    })
  }
}
