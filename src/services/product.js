import products from '../data/products.json'

export default {
  getAll: async ({ search, price, quantity } = {}) => {
    let results = products
    await new Promise((res) => setTimeout(res, 1000))
    if (search) {
      results = results.filter((el) => el.name.match(new RegExp(search, 'i')))
    }
    if (price) {
      results = results.filter(
        (el) => el.price >= price.min && el.price <= price.max
      )
    }
    if (quantity) {
      results = results.filter(
        (el) => el.qty >= quantity.min && el.qty <= quantity.max
      )
    }
    return results
  },
  updateById: () => {
    return 'success'
  },
  deleteById: () => {
    return 'success'
  }
}
