const fs = require('fs')
const faker = require('faker')

let products = []
for (let i = 0; i < 20; i++) {
  products.push({
    id: faker.datatype.number(),
    name: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    qty: Math.floor(Math.random() * 200),
    price: parseFloat(faker.commerce.price()),
    image: `${faker.image.animals()}?random=${i}`
  })
}
console.log(__dirname);
fs.writeFileSync(__dirname + '/products.json', JSON.stringify(products))
