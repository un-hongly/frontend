# soramitsu-assignment

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Pages
- Login Page
- Product Mangement Page

### Key Features
- Dynamic layouts
- Multi moduldes Vuex Store + Persisted State
- Authentication
- Router
- Content distribution with Slot
- Utility plugin
- Product CRUD
- Debounce function (Product Search)